import request from '@/utils/request'

export function loadDataUrl() {
  return 'admin-service/api/userMng/loadData'
}

export function deleteUrl() {
  return 'admin-service/api/userMng/doDelete'
}

export function toAdd(data) {
  return request({
    url: 'admin-service/api/userMng/toAdd',
    method: 'post',
    data
  })
}

export function toCopy(data) {
  return request({
    url: 'admin-service/api/userMng/toCopy',
    method: 'post',
    data
  })
}

export function viewMainData(data) {
  return request({
    url: 'admin-service/api/userMng/toView',
    method: 'post',
    data
  })
}

export function userRegister(data) {
  return request({
    url: 'admin-service/api/userMng/userRegister',
    method: 'post',
    data
  })
}

export function addOrEdit(data) {
  return request({
    url: 'admin-service/api/userMng/doSave',
    method: 'post',
    data
  })
}

export function del(data) {
  return request({
    url: 'admin-service/api/userMng/doDelete',
    method: 'post',
    data
  })
}

export function findUserByOrgCodeUrl() {
  return 'admin-service/api/userMng/findUserByOrgCode'
}

export function findUserByCpyCodeUrl() {
  return 'admin-service/api/userMng/findUserByCpyCode'
}

export function getCpyByUserAccount() {
  return request({
    url: 'admin-service/api/userMng/getCpyByUserAccount',
    method: 'get'
  })
}

export function getPrivOrgSetByUserAccount(cpyCode) {
  const data = { 'cpyCode': cpyCode }
  return request({
    url: 'admin-service/api/userMng/getPrivOrgSetByUserAccount',
    method: 'post',
    data
  })
}

export function updateCpyOrgSet(data) {
  return request({
    url: 'admin-service/api/userMng/updateCpyOrgSet',
    method: 'post',
    data
  })
}

export function updatePassword(data) {
  return request({
    url: 'admin-service/api/userMng/updatePassword',
    method: 'post',
    data
  })
}

export function updateEmail(code, data) {
  return request({
    url: 'api/users/updateEmail/' + code,
    method: 'post',
    data
  })
}

export function doSaveUserInfo(data) {
  return request({
    url: 'admin-service/api/userMng/doSaveUserInfo',
    method: 'post',
    data
  })
}

export function deleteByGridIdAndUser(gridId) {
  return request({
    url: 'admin-service/api/userMng/deleteByGridIdAndUser',
    method: 'get',
    params: {
      gridId
    }
  })
}

export function findByGridIdAndUser(gridId) {
  return request({
    url: 'admin-service/api/userMng/findByGridIdAndUser',
    method: 'get',
    params: {
      gridId
    }
  })
}

export function saveCustomField(data) {
  return request({
    url: 'admin-service/api/userMng/saveCustomField',
    method: 'post',
    data
  })
}
