// Exports the "forminput" plugin for usage with module loaders
// Usage:
//   CommonJS:
//     require('tinymce/plugins/forminput')
//   ES2015:
//     import 'tinymce/plugins/forminput'
require('./plugin.js');
