import Dict from './Dict'

const install = function(Vue) {
  Vue.mixin({
    data() {
      if (this.$options.dictTypes instanceof Array) {
        const dicts = {}
        return {
          dicts
        }
      }
      return {}
    },
    created() {
      if (this.$options.dictTypes instanceof Array) {
        new Dict(this.dicts).init(this.$options.dictTypes, () => {
          this.$nextTick(() => {
            this.$emit('dictReady')
          })
        })
      }
    }
  })
}

export default { install }
